﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWLootBoxService.Models {
    public class UserDropHistory {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int id { get; set; }
        public Guid user_id { get; set; }
        public DateTime date { get; set; }
        public Guid treasure_id { get; set; }
        [ForeignKey("treasure_id")]
        public Treasure treasure { get; set; }
        public Guid item_id { get; set; }
        [ForeignKey("item_id")]
        public Item item { get; set; }
        public int count { get; set; }
    }
}
