﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SWLootBoxService.Models {
    public class UserTreasureInfo {
        public Guid user_id { get; set; }
        public Guid treasure_id { get; set; }
        [ForeignKey("treasure_id")]
        public Treasure treasure { get; set; }
        public int count { get; set; }
    }
}
