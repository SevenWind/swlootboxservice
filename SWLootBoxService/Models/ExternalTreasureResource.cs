﻿using System;

namespace SWLootBoxService.Models {
    public class ExternalTreasureResource {
        public bool isDeleted { get; set; }
        public Guid item_id { get; set; }
        public int value { get; set; }
    }
}
