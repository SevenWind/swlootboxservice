﻿using SWLootBoxService.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SWLootBoxService.Models {
    public class TreasureResource {
        public Guid treasure_id { get; set; }
        [ForeignKey("treasure_id")]
        public Treasure treasure { get; set;}

        public Guid resource_id { get; set; }
        [ForeignKey("resource_id")]
        public Resource resource { get; set; }
    }
}
