﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWLootBoxService.Models {
    public class Character {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string image { get; set; }
        public int startStar { get; set; }

        public int rarity_id { get; set; }
        [ForeignKey("rarity_id")]
        public Rarity rarity { get; set; }

        public List<TreasureCharacter> treasures { get; set; }
    }
}
