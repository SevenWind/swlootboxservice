﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SWLootBoxService.Models {
    public class DropInfo {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid id { get; set; }
        public Guid item_id { get; set; }
        [ForeignKey("item_id")]
        public Item item { get; set; }
        public double chance { get; set; }
        public int count { get; set; }
        public int exp { get; set; }
        public int frequency_id { get; set; }
        [ForeignKey("frequency_id")]
        public ItemDropFrequency frequency { get; set; }
        public Guid treasure_id { get; set; }
        [ForeignKey("treasure_id")]
        public Treasure treasure { get; set; }
    }
}
