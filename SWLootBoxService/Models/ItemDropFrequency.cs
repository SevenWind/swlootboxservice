﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWLootBoxService.Models {
    public class ItemDropFrequency {
        [Key]
        public int id { get; set; }
        public string code { get; set; }
        public int value { get; set; }
    }
}
