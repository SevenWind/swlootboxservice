﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SWLootBoxService.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static SWLootBoxService.Models.Enums;

namespace SWLootBoxService.Models {
    public class SWLootBoxDbContext : DbContext {
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<ItemDropFrequency> ItemDropFrequency { get; set; }
        public virtual DbSet<Rarity> Rarity { get; set; }
        public virtual DbSet<Resource> Resources { get; set; }
        public virtual DbSet<DropInfo> DropInfos { get; set; }
        public virtual DbSet<Treasure> Treasures { get; set; }
        public virtual DbSet<TreasureResource> TreasureResources { get; set; }
        public virtual DbSet<UserTreasureInfo> UserTreasureInfos { get; set; }
        public virtual DbSet<UserDropHistory> UserDropHistory { get; set; }
        public virtual DbSet<StarFragment> StarFragments { get; set; }
        public virtual DbSet<Character> Characters { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
            optionsBuilder.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<TreasureResource>().HasKey(a => new { a.treasure_id, a.resource_id });

            modelBuilder.Entity<TreasureResource>()
               .HasOne(x => x.treasure)
               .WithMany(y => y.resources)
               .HasForeignKey(y => y.treasure_id);

            modelBuilder.Entity<TreasureResource>()
                .HasOne(x => x.resource)
                .WithMany(y => y.treasures)
                .HasForeignKey(y => y.resource_id);

            modelBuilder.Entity<TreasureCharacter>().HasKey(a => new { a.treasure_id, a.character_id });

            modelBuilder.Entity<TreasureCharacter>()
               .HasOne(x => x.treasure)
               .WithMany(y => y.characters)
               .HasForeignKey(y => y.treasure_id);

            modelBuilder.Entity<TreasureCharacter>()
                .HasOne(x => x.character)
                .WithMany(y => y.treasures)
                .HasForeignKey(y => y.character_id);

            modelBuilder.Entity<UserTreasureInfo>().HasKey(a => new { a.user_id, a.treasure_id });

            modelBuilder.Entity<UserTreasureInfo>()
               .HasOne(x => x.treasure)
               .WithMany(y => y.userOpenInfos)
               .HasForeignKey(y => y.treasure_id);

            Dictionary<ItemCode, Guid> itemIds = new Dictionary<ItemCode, Guid>();
            itemIds.Add(ItemCode.Coins, new Guid("e51be842-785e-4b21-9b6e-7e87a8557ade"));
            itemIds.Add(ItemCode.Gold, new Guid("89da2a28-a53d-4484-a1aa-8cf78552fbe2"));
            itemIds.Add(ItemCode.MagicAmulet, new Guid("c59df240-37dc-47c0-a2c0-dcaf2403d738"));
            itemIds.Add(ItemCode.ScrollOfHiring, new Guid("55ffb90f-5c4f-48b8-825d-0b96e0a96b1f"));

            itemIds.Add(ItemCode.GekkelKilnFragment, new Guid("c4993306-2a61-4302-84a0-5825d50d1ae8"));
            itemIds.Add(ItemCode.DornKirpichFragment, new Guid("10b34f8b-defe-44f4-9dbe-3c7e375b2b1a"));
            itemIds.Add(ItemCode.CerberFragment, new Guid("7dd83f78-ad3d-49c6-a284-d5c9c68bfe01"));
            itemIds.Add(ItemCode.TorilBuiniyFragment, new Guid("e2e0c07e-e82d-4c72-8dbe-abb0ec02b86f"));
            itemIds.Add(ItemCode.LiaAlleriaFragment, new Guid("75be5201-fa5c-40b5-9e81-a5f5a09311ac"));
            itemIds.Add(ItemCode.HemingRouterFragment, new Guid("055f174a-3963-43c3-8a86-bf043b3e22d8"));

            Dictionary<TreasureCode, Guid> treasureIds = new Dictionary<TreasureCode, Guid>();
            treasureIds.Add(TreasureCode.MercenaryTreasure, new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"));

            Dictionary<ResourceCode, Guid> resourceIds = new Dictionary<ResourceCode, Guid>();
            resourceIds.Add(ResourceCode.CoinsX10000, new Guid("ff6b59ba-7a4d-46b5-a0d4-9346b7b6199b"));
            resourceIds.Add(ResourceCode.CoinsX20000, new Guid("c5964be0-6417-4af0-8d10-72433f364a9a"));
            resourceIds.Add(ResourceCode.MagicAmuletX1, new Guid("2ae35aa7-6fea-49da-8993-4ec725fc91e1"));
            resourceIds.Add(ResourceCode.ScrollOfHiringX1, new Guid("799e140f-00db-44e3-a148-2b2088caff21"));

            FillRarity(modelBuilder);
            FillFrequency(modelBuilder);
            FillItems(modelBuilder, itemIds);
            FillResources(modelBuilder, itemIds, resourceIds);
            FillTreasures(modelBuilder, treasureIds);
            FillCharacter(modelBuilder);

            FillTreasureCharacters(modelBuilder, treasureIds);
            FillDropInfo(modelBuilder, itemIds, treasureIds);

            FillStarFragments(modelBuilder);
            FillTreasureResources(modelBuilder, treasureIds, resourceIds);
        }

        private void FillTreasureCharacters(ModelBuilder modelBuilder, Dictionary<TreasureCode, Guid> treasureIds) {
            modelBuilder.Entity<TreasureCharacter>().HasData(
                new TreasureCharacter() {
                    character_id = 1,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                },
                new TreasureCharacter() {
                    character_id = 2,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                },
                new TreasureCharacter() {
                    character_id = 3,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                },
                new TreasureCharacter() {
                    character_id = 4,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                },
                new TreasureCharacter() {
                    character_id = 5,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                },
                new TreasureCharacter() {
                    character_id = 6,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                }
            );
        }

        private void FillCharacter(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Character>().HasData(new Character() {
                id = (int)CharacterCode.GekkelKiln,
                image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "gekkel_kiln.jpg"))))),
                name = "Геккель Килн",
                rarity_id = 2,
                startStar = 2,
            },
            new Character() {
                id = (int)CharacterCode.DornKirpich,
                image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "dorn_kirpich.jpg"))))),
                name = "Дорн Кирпич",
                rarity_id = 2,
                startStar = 1,
            },
            new Character() {
                id = (int)CharacterCode.Cerber,
                image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "Cerber.jpg"))))),
                name = "Цербер",
                rarity_id = 2,
                startStar = 3,
            },
            new Character() {
                id = (int)CharacterCode.TorilBuiniy,
                image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "Toril_buiniy.jpg"))))),
                name = "Торил Буйный",
                rarity_id = 4,
                startStar = 3,
            },
            new Character() {
                id = (int)CharacterCode.LiaAlleria,
                image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "Lia_alleria.jpg"))))),
                name = "Лия Аллерия",
                rarity_id = 4,
                startStar = 3,
            },
            new Character() {
                id = (int)CharacterCode.HemingRouter,
                image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "hemming_router.png"))))),
                name = "Хеминг Роутер",
                rarity_id = 4,
                startStar = 3,
            });
        }

        private void FillStarFragments(ModelBuilder modelBuilder) {
            modelBuilder.Entity<StarFragment>().HasData(new StarFragment() {
                starCount = 1,
                needFragments = 10,
                borderColor = "#00FF7F"
            },
            new StarFragment() {
                starCount = 2,
                needFragments = 30,
                borderColor = "#40E0D0"
            },
            new StarFragment() {
                starCount = 3,
                needFragments = 80,
                borderColor = "#8A2BE2"
            },
            new StarFragment() {
                starCount = 4,
                needFragments = 150,
                borderColor = "#B8860B"
            },
            new StarFragment() {
                starCount = 5,
                needFragments = 300,
                borderColor = "#8B0000"
            });
        }

        private void FillRarity(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Rarity>().HasData(
                new Rarity() {
                    id = 1,
                    code = "None",
                },
                new Rarity() {
                    id = 2,
                    code = "COMMON",
                },
                new Rarity() {
                    id = 3,
                    code = "UNCOMMON",
                },
                new Rarity() {
                    id = 4,
                    code = "RARE",
                }
            );
        }

        private void FillFrequency(ModelBuilder modelBuilder) {
            modelBuilder.Entity<ItemDropFrequency>().HasData(
                new ItemDropFrequency() {
                    id = 1,
                    value = 1,
                    code = "COMMON",
                },
                 new ItemDropFrequency() {
                     id = 2,
                     value = 10,
                     code = "UNCOMMON",
                 },
                 new ItemDropFrequency() {
                     id = 3,
                     value = 50,
                     code = "RARE",
                 }
             );
        }

        private void FillItems(ModelBuilder modelBuilder, Dictionary<ItemCode, Guid> itemIds) {
            modelBuilder.Entity<Item>().HasData(
                new Item() {
                    id = itemIds[ItemCode.Coins],
                    name = "Монеты",
                    code = ItemCode.Coins.ToString(),
                    rarity_id = 2,
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("items", "coin.png"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.Gold],
                    name = "Золото",
                    rarity_id = 4,
                    code = ItemCode.Gold.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("items", "gold.png"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.ScrollOfHiring],
                    name = "Свиток Найма",
                    rarity_id = 1,
                    code = ItemCode.ScrollOfHiring.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("items", "scroll.png"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.MagicAmulet],
                    name = "Магический Амулет",
                    rarity_id = 1,
                    code = ItemCode.MagicAmulet.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("items", "amulet.png"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.GekkelKilnFragment],
                    name = "Фрагмент \"Геккель Килн\"",
                    rarity_id = 4,
                    characterId = 1,
                    code = "Character_" + ItemCode.GekkelKilnFragment.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "gekkel_kiln.jpg"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.DornKirpichFragment],
                    name = "Фрагмент \"Дорн Кирпич\"",
                    rarity_id = 4,
                    characterId = 2,
                    code = "Character_" + ItemCode.DornKirpichFragment.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "dorn_kirpich.jpg"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.CerberFragment],
                    name = "Фрагмент \"Цербер\"",
                    rarity_id = 4,
                    characterId = 3,
                    code = "Character_" + ItemCode.CerberFragment.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "Cerber.jpg"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.TorilBuiniyFragment],
                    name = "Фрагмент \"Торил Буйный\"",
                    rarity_id = 4,
                    characterId = 4,
                    code = "Character_" + ItemCode.TorilBuiniyFragment.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "Toril_buiniy.jpg"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.LiaAlleriaFragment],
                    name = "Фрагмент \"Лия Аллерия\"",
                    rarity_id = 4,
                    characterId = 5,
                    code = "Character_" + ItemCode.LiaAlleriaFragment.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "Lia_alleria.jpg"))))),
                },
                new Item() {
                    id = itemIds[ItemCode.HemingRouterFragment],
                    name = "Фрагмент \"Хеминг Роутер\"",
                    rarity_id = 4,
                    characterId = 6,
                    code = "Character_" + ItemCode.HemingRouterFragment.ToString(),
                    image = ImageHelpers.ImageToBase64(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("static", Path.Combine("images", Path.Combine("characters", "hemming_router.png"))))),
                }
            );
        }

        private void FillResources(ModelBuilder modelBuilder, Dictionary<ItemCode, Guid> itemIds, Dictionary<ResourceCode, Guid> resourceIds) {
            modelBuilder.Entity<Resource>().HasData(
                new Resource() {
                    id = resourceIds[ResourceCode.CoinsX10000],
                    item_id = itemIds[ItemCode.Coins],
                    value = 10000,
                },
                new Resource() {
                    id = resourceIds[ResourceCode.CoinsX20000],
                    item_id = itemIds[ItemCode.Coins],
                    value = 20000,
                },
                new Resource() {
                    id = resourceIds[ResourceCode.ScrollOfHiringX1],
                    item_id = itemIds[ItemCode.ScrollOfHiring],
                    value = 1,
                },
                new Resource() {
                    id = resourceIds[ResourceCode.MagicAmuletX1],
                    item_id = itemIds[ItemCode.MagicAmulet],
                    value = 1,
                }
            );
        }

        private void FillTreasures(ModelBuilder modelBuilder, Dictionary<TreasureCode, Guid> treasureIds) {
            modelBuilder.Entity<Treasure>().HasData(
                new Treasure() {
                    id = treasureIds[TreasureCode.MercenaryTreasure],
                    name = "Сундук Наёмников",
                    code = TreasureCode.MercenaryTreasure.ToString(),
                    image = "../assets/treasure.png",
                    exp = 100,
                }
            );
        }

        private void FillTreasureResources(ModelBuilder modelBuilder, Dictionary<TreasureCode, Guid> treasureIds, Dictionary<ResourceCode, Guid> resourceIds) {
            modelBuilder.Entity<TreasureResource>().HasData(
                    new TreasureResource() {
                        treasure_id = treasureIds[TreasureCode.MercenaryTreasure],
                        resource_id = resourceIds[ResourceCode.CoinsX10000]
                    },
                    new TreasureResource() {
                        treasure_id = treasureIds[TreasureCode.MercenaryTreasure],
                        resource_id = resourceIds[ResourceCode.ScrollOfHiringX1]
                    }
            );
        }

        private void FillDropInfo(ModelBuilder modelBuilder, Dictionary<ItemCode, Guid> itemIds, Dictionary<TreasureCode, Guid> treasureIds) {
            modelBuilder.Entity<DropInfo>().HasData(
                new DropInfo() {
                    id = new Guid("3974b24c-17ed-4877-8086-6cf2011c3daf"),
                    item_id = itemIds[ItemCode.Coins],
                    frequency_id = 1,
                    chance = 95,
                    count = 1000,
                    exp = 10,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                },
                new DropInfo() {
                    id = new Guid("e8cbb863-a423-4ba3-9c59-2ad2622ab1b8"),
                    item_id = itemIds[ItemCode.Gold],
                    frequency_id = 1,
                    chance = 5,
                    count = 10,
                    exp = 100,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                },
                new DropInfo() {
                    id = new Guid("0172b712-96ae-46f3-a439-02a29f1c95b9"),
                    item_id = itemIds[ItemCode.GekkelKilnFragment],
                    frequency_id = 2,
                    chance = 33,
                    count = 10,
                    exp = 50,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                },
                new DropInfo() {
                    id = new Guid("6c8b1b8e-e427-4261-96ad-804b4ee8a2f4"),
                    item_id = itemIds[ItemCode.DornKirpichFragment],
                    frequency_id = 2,
                    chance = 34,
                    count = 10,
                    exp = 50,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                },
                new DropInfo() {
                    id = new Guid("749ad6da-8133-4edb-a17e-d4690959380a"),
                    item_id = itemIds[ItemCode.CerberFragment],
                    frequency_id = 2,
                    chance = 33,
                    count = 10,
                    exp = 50,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                },
                new DropInfo() {
                    id = new Guid("f9faac8b-a0da-414c-a905-ebd52fb22a9c"),
                    item_id = itemIds[ItemCode.TorilBuiniyFragment],
                    frequency_id = 3,
                    chance = 34,
                    count = 10,
                    exp = 100,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                },
                new DropInfo() {
                    id = new Guid("cc19d80e-ff73-49f0-bfb7-0047bf87d936"),
                    item_id = itemIds[ItemCode.LiaAlleriaFragment],
                    frequency_id = 3,
                    chance = 33,
                    count = 10,
                    exp = 100,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                },
                new DropInfo() {
                    id = new Guid("5d08b25b-1fcc-4e71-a2ea-1677b8f502db"),
                    item_id = itemIds[ItemCode.HemingRouterFragment],
                    frequency_id = 3,
                    chance = 33,
                    count = 10,
                    exp = 100,
                    treasure_id = treasureIds[TreasureCode.MercenaryTreasure]
                }
            );
        }
    }
}
