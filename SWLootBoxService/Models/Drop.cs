﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SWLootBoxService.Models {
    public class Drop {
        public Item item { get; set; }
        public int value { get; set; }
        public int exp { get; set; }
    }
}
