﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWLootBoxService.Models {
    public class Resource {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid id { get; set; }
        public Guid item_id { get; set; }
        [ForeignKey("item_id")]
        public Item item { get; set; }
        public int value { get; set; }
        public virtual List<TreasureResource> treasures { get; set; }
    }
}
