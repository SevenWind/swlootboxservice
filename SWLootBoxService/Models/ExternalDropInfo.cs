using System;

namespace SWLootBoxService.Models {
    public class ExternalDropInfo {
        public bool isDeleted { get; set; }
        public int frequency { get; set; }
        public int chance { get; set; }
        public Guid item_id { get; set; }
        public int count { get; set; }
        public int exp { get; set; }
    }
}