﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWLootBoxService.Models {
    public class TreasureCharacter {
        public Guid treasure_id { get; set; }
        [ForeignKey("treasure_id")]
        public Treasure treasure { get; set; }

        public int character_id { get; set; }
        [ForeignKey("character_id")]
        public Character character { get; set; }
    }
}
