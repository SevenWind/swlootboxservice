﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SWLootBoxService.Models {
    public class Enums {
        public enum ItemCode {
            Coins = 1,
            Gold,
            ScrollOfHiring,
            MagicAmulet,
            GekkelKilnFragment,
            DornKirpichFragment,
            CerberFragment,
            TorilBuiniyFragment,
            LiaAlleriaFragment,
            HemingRouterFragment,
        }

        public enum TreasureCode {
            MercenaryTreasure = 1,
            HeartOfMagicTreasure,
            HolyCircleTreasure,
            ApostateTreasure,
            NightBrotherhoodTreasure
        }

        public enum ResourceCode {
            CoinsX10000 = 1,
            CoinsX20000,
            ScrollOfHiringX1,
            MagicAmuletX1,
        }

        public enum CharacterCode {
            GekkelKiln = 1,
            DornKirpich = 2,
            Cerber = 3,
            TorilBuiniy = 4,
            LiaAlleria = 5,
            HemingRouter = 6,
        }

        public static class AuthRole {
            public const string ADMIN = "Admin";
            public const string PLAYER = "Player";
            public const string TESTER = "Tester";
            public const string ANY = "Admin, Player, Tester";
        }
    }
}
