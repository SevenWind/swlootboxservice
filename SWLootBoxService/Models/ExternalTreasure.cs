﻿using System;
using System.Collections.Generic;

namespace SWLootBoxService.Models {
    public class ExternalTreasure {
        public Guid id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string image { get; set; }
        public int exp { get; set; }
        public ExternalTreasureResource[] resources { get; set; }
    }
}
