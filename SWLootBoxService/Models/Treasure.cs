﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SWLootBoxService.Models {
    public class Treasure {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string image { get; set; }
        public int exp { get; set; }
        public virtual List<TreasureResource> resources { get; set; }
        public virtual List<UserTreasureInfo> userOpenInfos { get; set; }
        public List<DropInfo> dropInfos { get; set; }
        public List<TreasureCharacter> characters { get; set; }
        public bool isLocked { get; set; }
    }
}
