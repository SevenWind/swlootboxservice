﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SWLootBoxService.Models;
using SWLootBoxService.Utility;
using static SWLootBoxService.Models.Enums;

namespace SWLootBoxService.Controllers {
    [Route ("api/v1/[controller]/[action]")]
    [Authorize (Roles = AuthRole.ADMIN, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class AdminController : ControllerBase {

        private readonly ILogger<AdminController> _logger;
        private SWLootBoxDbContext db;

        public AdminController (ILogger<AdminController> logger, SWLootBoxDbContext _db) {
            _logger = logger;
            db = _db;
        }

        /// <summary>
        /// Получение списка сундуков для админки
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> List () {
            try {
                var treasures = await db.Treasures
                    .Include (a => a.resources).ToListAsync ();
                var trsCount = treasures.Count;
                for (int i = 0; i < trsCount; i++) {
                    DBHelpers.InclureTreasureResources (db, treasures[i]);
                }

                return Ok (JsonHelpers.SerializeObjectWithLoopIgnore (treasures));
            } catch (Exception e) {
                _logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Блокировка сунудука - чтобы он не отображался у пользователей
        /// </summary>
        /// <param name="treasureId">ИД сундука</param>
        [HttpPost ("{treasureId}")]
        public async Task<IActionResult> LockTreasure (Guid treasureId) {
            try {
                var treasure = await db.Treasures.FindAsync (treasureId);
                if (treasure == null) {
                    return NotFound ("Сундук не найден");
                }
                treasure.isLocked = true;
                await db.SaveChangesAsync ();
                return Ok (true);
            } catch (Exception e) {
                _logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Разблокировка сундука (появится у пользователей)
        /// </summary>
        /// <param name="treasureId">ИД сундука</param>
        [HttpPost ("{treasureId}")]
        public async Task<IActionResult> UnLockTreasure (Guid treasureId) {
            try {
                var treasure = await db.Treasures.FindAsync (treasureId);
                if (treasure == null) {
                    return NotFound ("Сундук не найден");
                }
                treasure.isLocked = false;
                await db.SaveChangesAsync ();
                return Ok (true);
            } catch (Exception e) {
                _logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Сохранение сундука
        /// </summary>
        /// <param name="treasure">модель сундука</param>
        [HttpPost]
        public async Task<IActionResult> SaveTreasure ([FromBody] ExternalTreasure treasure) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var dbTreasure = await db.Treasures.FindAsync (treasure.id);
                    if (dbTreasure == null) {
                        dbTreasure = new Treasure () {
                        id = Guid.NewGuid (),
                        name = treasure.name,
                        exp = treasure.exp,
                        image = treasure.image,
                        isLocked = true
                        };
                        await db.Treasures.AddAsync (dbTreasure);
                        await db.SaveChangesAsync ();
                        tr.Commit ();
                        return Ok (true);
                    }
                    if (!string.IsNullOrEmpty (treasure.image) || treasure.image != "_") {
                        dbTreasure.image = treasure.image;
                    }
                    dbTreasure.name = treasure.name;
                    dbTreasure.exp = treasure.exp;
                    var resCount = treasure.resources.Length;
#warning Продумать цикл. Возможно, можно оптимизировать
                    for (int i = 0; i < resCount; i++) {
                        var resource = treasure.resources[i];
                        var item = await db.Items.FindAsync (resource.item_id);
                        if (item == null) {
                            return NotFound ("Не найден ресурс");
                        }
                        if (item.isDeleted) {
                            return BadRequest ("Ресурс " + item.name + " был удален. Выберите другой.");
                        }
                        // Нужный ресурс есть в бд?
                        var dbRes = await db.Resources.FirstOrDefaultAsync (a => a.item_id == item.id && a.value == resource.value);
                        if (dbRes != null) {
                            // Связан ли он с сундуком?
                            var resLink = await db.TreasureResources.FirstOrDefaultAsync (a => a.resource_id == dbRes.id && a.treasure_id == treasure.id);
                            if (resLink != null) {
                                // Иначе продолжаем цикл по ресурсам
                                if (resource.isDeleted) {
                                    db.TreasureResources.Remove (resLink);
                                    if (await db.TreasureResources.AnyAsync (a => a.resource_id == resLink.resource_id) == false) {
                                        db.Resources.Remove (dbRes);
                                    }
                                }
                            } else {
                                // добавляем связь
                                await db.TreasureResources.AddAsync (new TreasureResource () {
                                    resource_id = dbRes.id,
                                        treasure_id = treasure.id
                                });
                            }
                        } else {
                            // Создаем новый ресурс
                            dbRes = new Resource () {
                                id = Guid.NewGuid (),
                                item_id = item.id,
                                value = resource.value
                            };
                            await db.Resources.AddAsync (dbRes);
                            // и связь меж ним и сундуком
                            await db.TreasureResources.AddAsync (new TreasureResource () {
                                resource_id = dbRes.id,
                                    treasure_id = treasure.id
                            });
                        }
                    }
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok (true);
                }
            } catch (Exception e) {
                _logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Сохранение списка выпадающих предметов
        /// </summary>
        /// <param name="treasureId">ИД сундука</param>
        /// <param name="dropInfo">Массив моделей информации о выпадающем предмете</param>
        [HttpPost ("{treasureId}")]
        public async Task<IActionResult> SaveDropInfo (Guid treasureId, [FromBody] ExternalDropInfo[] dropInfo) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var dbTreasure = await db.Treasures.FindAsync (treasureId);
                    if (dbTreasure == null) {
                        return NotFound ("Не найден сундук");
                    }
                    // Проверка на то что сумма опыта для каждого freequency == 100
                    var freequencyGroups = dropInfo.Where (a => a.isDeleted == false).GroupBy (a => a.frequency).ToArray ();
                    var groupsCount = freequencyGroups.Length;
                    for (int i = 0; i < groupsCount; i++) {
                        double groupChanceSum = 0;
                        foreach (var info in freequencyGroups[i]) {
                            groupChanceSum += info.chance;
                        }
                        if (groupChanceSum != 100) {
                            return BadRequest ("Сумма шансов для редкости " + freequencyGroups[i].Key + " не равна 100%");
                        }
                    }

                    var freequencies = new Dictionary<int, int> ();
                    freequencies.Add (1, 1);
                    freequencies.Add (10, 2);
                    freequencies.Add (50, 3);

                    var dropCount = dropInfo.Length;
                    for (int i = 0; i < dropCount; i++) {
                        var extDrop = dropInfo[i];
                        var dbDrop = await db.DropInfos.FirstOrDefaultAsync (a => a.item_id == extDrop.item_id && a.treasure_id == dbTreasure.id);
                        if (dbDrop == null) {
                            var item = await db.Items.FindAsync (extDrop.item_id);
                            if (item == null) {
                                return NotFound ("Не найден предмет");
                            }
                            if (item.isDeleted) {
                                return BadRequest ("Предмет " + item.name + " был удален. Выберите другой.");
                            }
                            dbDrop = new DropInfo () {
                                id = Guid.NewGuid (),
                                count = extDrop.count,
                                chance = extDrop.chance,
                                exp = extDrop.exp,
                                frequency_id = freequencies[extDrop.frequency],
                                item_id = item.id,
                                treasure_id = dbTreasure.id
                            };
                            await db.DropInfos.AddAsync (dbDrop);
                        } else {
                            if (extDrop.isDeleted) {
                                db.DropInfos.Remove (dbDrop);
                            } else {
                                dbDrop.chance = extDrop.chance;
                                dbDrop.count = extDrop.count;
                                dbDrop.exp = extDrop.exp;
                                dbDrop.frequency_id = freequencies[extDrop.frequency];
                            }
                        }
                    }
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok (true);
                }
            } catch (Exception e) {
                _logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Удаление предмета
        /// </summary>
        /// <param name="itemId">ИД предмета</param>
        [HttpPost ("{itemId}")]
        public async Task<IActionResult> DeleteItem (Guid itemId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var item = db.Items.Find (itemId);
                    if (item == null) {
                        return NotFound ("Предмет не найден");
                    }
                    item.isDeleted = true;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                _logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Восстановление предмета
        /// </summary>
        /// <param name="itemId">ИД предмета</param>
        [HttpPost ("{itemId}")]
        public async Task<IActionResult> RestoreItem (Guid itemId) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var item = db.Items.Find (itemId);
                    if (item == null) {
                        return NotFound ("Предмет не найден");
                    }
                    item.isDeleted = false;
                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                _logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Сохранение предмета
        /// </summary>
        /// <param name="item">Модель предмета</param>
        [HttpPost]
        public async Task<IActionResult> SaveItem ([FromBody] Item item) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var dbItem = await db.Items.FirstOrDefaultAsync (a => a.id == item.id);
                    if (dbItem == null) {
                        // create new
                        dbItem = new Item ();
                        dbItem.id = item.id;
                        dbItem.name = item.name;
                        dbItem.code = item.code;
                        dbItem.image = item.image;
                        dbItem.rarity_id = item.rarity_id;
                        dbItem.characterId = item.characterId;
                        await db.Items.AddAsync (dbItem);
                        await db.SaveChangesAsync ();
                        tr.Commit ();
                        return Ok ();
                    }
                    // update
                    dbItem.name = item.name;
                    dbItem.code = item.code;
                    if (!string.IsNullOrWhiteSpace (item.image) && item.image != dbItem.image)
                        dbItem.image = item.image;
                    dbItem.rarity_id = item.rarity_id;
                    dbItem.characterId = item.characterId;

                    await db.SaveChangesAsync ();
                    tr.Commit ();
                    return Ok ();
                }
            } catch (Exception e) {
                _logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }
    }
}