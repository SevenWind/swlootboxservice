﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SWLootBoxService.Models;
using System;
using System.Linq;
using static SWLootBoxService.Models.Enums;

namespace SWLootBoxService.Controllers {
    [Route("api/v1/[controller]/[action]")]
    [ApiController]
    public class CollectionController : ControllerBase {

        private readonly ILogger<CollectionController> logger;
        private SWLootBoxDbContext db;

        public CollectionController(ILogger<CollectionController> _logger, SWLootBoxDbContext _db) {
            logger = _logger;
            db = _db;
        }

        /// <summary>
        /// Получение списка доступных персонажей
        /// </summary>
        [HttpGet]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetCollection() {
            try {
                var collection = db.Characters.Include(a=>a.rarity).Include(a => a.treasures).Select(a=> new {
                    a.id,
                    a.name,
                    a.image,
                    a.startStar,
                    a.rarity.code,
                    treasures = a.treasures.Select(tr => new {
                        tr.treasure.id,
                        tr.treasure.name,
                        tr.treasure.code,
                        tr.treasure.image,
                    })
                }).ToList();
                return Ok(collection);
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Получение модели рейтинга персонажей
        /// </summary>
        [HttpGet]
        [Authorize(Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetStars() {
            try {
                var stars = db.StarFragments.ToList();
                return Ok(stars);
            } catch (Exception e) {
                logger.LogError(default(EventId), e, e.Message);
                return StatusCode(500);
            }
        }
    }
}