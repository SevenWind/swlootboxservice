﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SWLootBoxService.Models;
using SWLootBoxService.Services;
using SWLootBoxService.Utility;
using static SWLootBoxService.Models.Enums;

namespace SWLootBoxService.Controllers {
    [Route ("api/v1/[controller]/[action]")]
    [ApiController]
    public class LootBoxController : ControllerBase {

        private readonly ILogger<LootBoxController> _logger;
        private readonly IRandom _rnd;
        private SWLootBoxDbContext db;

        public LootBoxController (ILogger<LootBoxController> logger, IRandom random, SWLootBoxDbContext _db) {
            _logger = logger;
            _rnd = random;
            db = _db;
        }

        /// <summary>
        /// Получения списка сундуков
        /// </summary>
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet]
        public async Task<IActionResult> List () {
            try {
                var treasures = await db.Treasures
                    .Include (a => a.resources).Where (a => a.isLocked == false).ToListAsync ();
                var trsCount = treasures.Count;
                for (int i = 0; i < trsCount; i++) {
                    DBHelpers.InclureTreasureResources (db, treasures[i]);
                }

                return Ok (JsonHelpers.SerializeObjectWithLoopIgnore (treasures));
            } catch (Exception e) {
                _logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Получение списка выпадающих предметов сундука
        /// </summary>
        /// <param name="id">ИД сундука</param>
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet ("{id}")]
        public async Task<IActionResult> GetDropInfo (Guid id) {
            try {
                var treasure = await db.Treasures.Include (a => a.dropInfos).FirstOrDefaultAsync (a => a.id == id);
                if (treasure == null)
                    return NotFound ();
                DBHelpers.InclureTreasureDropInfos (db, treasure);
                treasure.dropInfos = treasure.dropInfos.Where (a => a.item.isDeleted == false).OrderBy (a => a.frequency.value).ToList ();
                return Ok (JsonHelpers.SerializeObjectWithLoopIgnore (treasure));
            } catch (Exception e) {
                _logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Открытие сундука 
        /// </summary>
        /// <param name="userId">ИД юзера, кто открывает</param>
        /// <param name="treasureId">ИД сундука</param>
        /// <param name="count">количество открытий</param>
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet ("userId={userid}&treasureId={treasureid}&count={count}")]
        public async Task<IActionResult> Open (Guid userId, Guid treasureId, int count) {
            try {
                using (var tr = await db.Database.BeginTransactionAsync ()) {
                    var treasure = await db.Treasures.Include (a => a.dropInfos).Include (a => a.userOpenInfos).FirstOrDefaultAsync (a => a.id == treasureId);
                    if (treasure == null)
                        return NotFound ();
                    DBHelpers.InclureTreasureDropInfos (db, treasure);
                    var userRollInfo = treasure.userOpenInfos.FirstOrDefault (a => a.user_id == userId);
                    if (userRollInfo == null) {
                        userRollInfo = new UserTreasureInfo () { count = 0, treasure_id = treasure.id, user_id = userId };
                        await db.UserTreasureInfos.AddAsync (userRollInfo);
                        await db.SaveChangesAsync ();
                    }

                    var frequency = treasure.dropInfos.Select (a => a.frequency).Distinct ().OrderByDescending (a => a.value).ToArray ();
                    var frequencyCount = frequency.Length;
                    var droppedItems = new List<Drop> ();

                    for (int i = 0; i < count; i++) {
#warning обернуть в функцию, возможно, оптимизировать
                        userRollInfo.count++;
                        var drop = LootBoxHelper.OpenLootBox (treasure, frequency, frequencyCount, userRollInfo.count, _rnd);
                        droppedItems.Add (drop);

                        await db.UserDropHistory.AddAsync (new UserDropHistory () {
                            user_id = userId,
                                date = DateTime.Now,
                                treasure_id = treasure.id,
                                item_id = drop.item.id,
                                count = drop.value,
                        });
                        await db.SaveChangesAsync ();
                    }
                    tr.Commit ();
                    return Ok (JsonHelpers.SerializeObjectWithLoopIgnore (droppedItems));
                }
            } catch (Exception e) {
                _logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }

        /// <summary>
        /// Получение истории открытий сундуков с пагинацией
        /// </summary>
        /// <param name="userId">ИД пользователя</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">размер страницы</param>
        [Authorize (Roles = AuthRole.ANY, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet ("userId={userid}&pageNumber={pageNumber}&pageSize={pageSize}")]
        public async Task<IActionResult> GetHist (Guid userId, int pageNumber, int pageSize) {
            try {
                var fullHistory = db.UserDropHistory.Where (a => a.user_id == userId).Include (a => a.treasure).Include (a => a.item).Include (a => a.item.rarity);
                var history = await fullHistory
                    .OrderByDescending (a => a.date)
                    .Skip ((pageNumber - 1) * pageSize).Take (pageSize)
                    .Select (a => new {
                        a.id,
                            item = a.item.name,
                            a.item.image,
                            rarity = a.item.rarity.code,
                            treasure_name = a.treasure.name,
                            a.date,
                            a.count,
                    })
                    .ToArrayAsync ();
                return Ok (JsonHelpers.SerializeObjectWithLoopIgnore (new {
                    history,
                    pageCount = (int) Math.Ceiling ((double) fullHistory.Count () / pageSize),
                    pageNumber
                }));
            } catch (Exception e) {
                _logger.LogError (default (EventId), e, e.Message);
                return StatusCode (500);
            }
        }
    }
}