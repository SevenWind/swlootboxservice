﻿using SWLootBoxService.Services;

namespace SWLootBoxService.Utility {
    public static class MathHelpers {
        /// <summary>
        /// Получение случайного индекса массива сучетом весов
        /// </summary>
        /// <param name="weights">массив вероятностных весов</param>
        /// <param name="_rnd">рандомизатор</param>
        /// <returns>случайны индекс</returns>
        public static int GetRandomWeightedIndex(int[] weights, IRandom _rnd) {
            int weightSum = 0;
            for (int i = 0; i < weights.Length; ++i) {
                weightSum += weights[i];
            }

            int index = 0;
            int lastIndex = weights.Length - 1;
            while (index < lastIndex) {
                if (_rnd.Next(0, weightSum) < weights[index]) {
                    return index;
                }
                weightSum -= weights[index++];
            }

            return index;
        }
    }
}
