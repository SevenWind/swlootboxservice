﻿using System;
using System.Linq;
using SWLootBoxService.Models;
using SWLootBoxService.Services;

namespace SWLootBoxService.Utility {
    public static class LootBoxHelper {

        public static Drop OpenLootBox(Treasure treasure, ItemDropFrequency[] frequency, int frequencyCount, int userRollCount, IRandom _rnd) {
            for (int j = 0; j < frequencyCount; j++) {
                if (userRollCount % frequency[j].value == 0) {
                    var drops = treasure.dropInfos.Where(a => a.frequency_id == frequency[j].id && a.item.isDeleted == false).OrderBy(a => a.chance).ToArray();
                    var index = MathHelpers.GetRandomWeightedIndex(drops.Select(a => (int)Math.Floor(a.chance * 100)).ToArray(), _rnd);
                    return new Drop() { item = drops[index].item, value = drops[index].count, exp = drops[index].exp };
                }
            }
            return null;
        }
    }
}
