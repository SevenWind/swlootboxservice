﻿using System;
using System.IO;

namespace SWLootBoxService.Utility {
    public class ImageHelpers {
        /// <summary>
        /// Конвертация изображения в base64
        /// </summary>
        /// <param name="path">путь к изображению</param>
        /// <returns>base64 string</returns>
        public static string ImageToBase64(string path) {
            byte[] b = File.ReadAllBytes(path);
            return "data:image/png;base64," + Convert.ToBase64String(b);
        }
    }
}
