﻿using SWLootBoxService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SWLootBoxService.Utility {
    /// <summary>
    /// Хелперы для работы с БД
    /// </summary>
    public static class DBHelpers {
        /// <summary>
        /// Подключение таблиц для отображения данных об информации о дропе с сундука
        /// </summary>
        /// <param name="db">контекст бд</param>
        /// <param name="treasure">Объект сундука</param>
        public static void InclureTreasureDropInfos(SWLootBoxDbContext db, Treasure treasure) {
            var dropInfos = treasure.dropInfos;
            var dropInfosCount = dropInfos.Count;
            for (int j = 0; j < dropInfosCount; j++) {
                db.Entry(dropInfos[j]).Reference(a => a.item).Load();
                db.Entry(dropInfos[j]).Reference(a => a.frequency).Load();
                db.Entry(dropInfos[j].item).Reference(a => a.rarity).Load();
            }
        }
        /// <summary>
        /// Подключение таблиц для отображения данных об информации о ресурсах для открытия сундука
        /// </summary>
        /// <param name="db">контекст бд</param>
        /// <param name="treasure">Объект сундука</param>
        public static void InclureTreasureResources(SWLootBoxDbContext db, Treasure treasure) {
            var resources = treasure.resources;
            var resourcesCount = resources.Count;
            for (int j = 0; j < resourcesCount; j++) {
                db.Entry(resources[j]).Reference(a => a.resource).Load();
                db.Entry(resources[j].resource).Reference(a => a.item).Load();
                db.Entry(resources[j].resource.item).Reference(a => a.rarity).Load();
            }
        }
    }
}
