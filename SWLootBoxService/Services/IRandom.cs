﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SWLootBoxService.Services {
    public interface IRandom {
        int Next(int start, int end);
    }
}
