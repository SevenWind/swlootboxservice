﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SWLootBoxService.Services {
    public class RandomService : IRandom {
        static Random Rnd = new Random();

        public int Next(int start, int end) {
            return Rnd.Next(start, end);
        }
    }
}
