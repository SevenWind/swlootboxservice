﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SWLootBoxService.Migrations
{
    public partial class manytomanytreaschars : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Characters_Treasures_treasure_id",
                table: "Characters");

            migrationBuilder.DropIndex(
                name: "IX_Characters_treasure_id",
                table: "Characters");

            migrationBuilder.DropColumn(
                name: "treasure_id",
                table: "Characters");

            migrationBuilder.CreateTable(
                name: "TreasureCharacter",
                columns: table => new
                {
                    treasure_id = table.Column<Guid>(nullable: false),
                    character_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TreasureCharacter", x => new { x.treasure_id, x.character_id });
                    table.ForeignKey(
                        name: "FK_TreasureCharacter_Characters_character_id",
                        column: x => x.character_id,
                        principalTable: "Characters",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TreasureCharacter_Treasures_treasure_id",
                        column: x => x.treasure_id,
                        principalTable: "Treasures",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "TreasureCharacter",
                columns: new[] { "treasure_id", "character_id" },
                values: new object[,]
                {
                    { new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"), 1 },
                    { new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"), 2 },
                    { new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"), 3 },
                    { new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"), 4 },
                    { new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"), 5 },
                    { new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"), 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_TreasureCharacter_character_id",
                table: "TreasureCharacter",
                column: "character_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TreasureCharacter");

            migrationBuilder.AddColumn<Guid>(
                name: "treasure_id",
                table: "Characters",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "id",
                keyValue: 1,
                column: "treasure_id",
                value: new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"));

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "id",
                keyValue: 2,
                column: "treasure_id",
                value: new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"));

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "id",
                keyValue: 3,
                column: "treasure_id",
                value: new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"));

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "id",
                keyValue: 4,
                column: "treasure_id",
                value: new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"));

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "id",
                keyValue: 5,
                column: "treasure_id",
                value: new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"));

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "id",
                keyValue: 6,
                column: "treasure_id",
                value: new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"));

            migrationBuilder.CreateIndex(
                name: "IX_Characters_treasure_id",
                table: "Characters",
                column: "treasure_id");

            migrationBuilder.AddForeignKey(
                name: "FK_Characters_Treasures_treasure_id",
                table: "Characters",
                column: "treasure_id",
                principalTable: "Treasures",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
