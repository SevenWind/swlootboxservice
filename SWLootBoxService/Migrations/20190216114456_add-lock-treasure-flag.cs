﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SWLootBoxService.Migrations
{
    public partial class addlocktreasureflag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isLocked",
                table: "Treasures",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "DropInfos",
                keyColumn: "id",
                keyValue: new Guid("0172b712-96ae-46f3-a439-02a29f1c95b9"),
                column: "exp",
                value: 50);

            migrationBuilder.UpdateData(
                table: "DropInfos",
                keyColumn: "id",
                keyValue: new Guid("5d08b25b-1fcc-4e71-a2ea-1677b8f502db"),
                column: "exp",
                value: 100);

            migrationBuilder.UpdateData(
                table: "DropInfos",
                keyColumn: "id",
                keyValue: new Guid("6c8b1b8e-e427-4261-96ad-804b4ee8a2f4"),
                column: "exp",
                value: 50);

            migrationBuilder.UpdateData(
                table: "DropInfos",
                keyColumn: "id",
                keyValue: new Guid("749ad6da-8133-4edb-a17e-d4690959380a"),
                column: "exp",
                value: 50);

            migrationBuilder.UpdateData(
                table: "DropInfos",
                keyColumn: "id",
                keyValue: new Guid("cc19d80e-ff73-49f0-bfb7-0047bf87d936"),
                column: "exp",
                value: 100);

            migrationBuilder.UpdateData(
                table: "DropInfos",
                keyColumn: "id",
                keyValue: new Guid("f9faac8b-a0da-414c-a905-ebd52fb22a9c"),
                column: "exp",
                value: 100);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isLocked",
                table: "Treasures");

            migrationBuilder.UpdateData(
                table: "DropInfos",
                keyColumn: "id",
                keyValue: new Guid("0172b712-96ae-46f3-a439-02a29f1c95b9"),
                column: "exp",
                value: 70);

            migrationBuilder.UpdateData(
                table: "DropInfos",
                keyColumn: "id",
                keyValue: new Guid("5d08b25b-1fcc-4e71-a2ea-1677b8f502db"),
                column: "exp",
                value: 200);

            migrationBuilder.UpdateData(
                table: "DropInfos",
                keyColumn: "id",
                keyValue: new Guid("6c8b1b8e-e427-4261-96ad-804b4ee8a2f4"),
                column: "exp",
                value: 70);

            migrationBuilder.UpdateData(
                table: "DropInfos",
                keyColumn: "id",
                keyValue: new Guid("749ad6da-8133-4edb-a17e-d4690959380a"),
                column: "exp",
                value: 70);

            migrationBuilder.UpdateData(
                table: "DropInfos",
                keyColumn: "id",
                keyValue: new Guid("cc19d80e-ff73-49f0-bfb7-0047bf87d936"),
                column: "exp",
                value: 200);

            migrationBuilder.UpdateData(
                table: "DropInfos",
                keyColumn: "id",
                keyValue: new Guid("f9faac8b-a0da-414c-a905-ebd52fb22a9c"),
                column: "exp",
                value: 200);
        }
    }
}
