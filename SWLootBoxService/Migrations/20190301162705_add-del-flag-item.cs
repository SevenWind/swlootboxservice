﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SWLootBoxService.Migrations
{
    public partial class adddelflagitem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isDeleted",
                table: "Items",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Treasures",
                keyColumn: "id",
                keyValue: new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"),
                column: "image",
                value: "../assets/treasure.png");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isDeleted",
                table: "Items");

            migrationBuilder.UpdateData(
                table: "Treasures",
                keyColumn: "id",
                keyValue: new Guid("ffe92dc7-5929-4b6e-8035-b1996ed222ba"),
                column: "image",
                value: "treasure.png");
        }
    }
}
