﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SWLootBoxService.Migrations
{
    public partial class changefragcodes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("055f174a-3963-43c3-8a86-bf043b3e22d8"),
                column: "code",
                value: "Character_HemingRouterFragment");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("10b34f8b-defe-44f4-9dbe-3c7e375b2b1a"),
                column: "code",
                value: "Character_DornKirpichFragment");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("75be5201-fa5c-40b5-9e81-a5f5a09311ac"),
                column: "code",
                value: "Character_LiaAlleriaFragment");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("7dd83f78-ad3d-49c6-a284-d5c9c68bfe01"),
                column: "code",
                value: "Character_CerberFragment");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("c4993306-2a61-4302-84a0-5825d50d1ae8"),
                column: "code",
                value: "Character_GekkelKilnFragment");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("e2e0c07e-e82d-4c72-8dbe-abb0ec02b86f"),
                column: "code",
                value: "Character_TorilBuiniyFragment");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("055f174a-3963-43c3-8a86-bf043b3e22d8"),
                column: "code",
                value: "HemingRouterFragment");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("10b34f8b-defe-44f4-9dbe-3c7e375b2b1a"),
                column: "code",
                value: "DornKirpichFragment");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("75be5201-fa5c-40b5-9e81-a5f5a09311ac"),
                column: "code",
                value: "LiaAlleriaFragment");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("7dd83f78-ad3d-49c6-a284-d5c9c68bfe01"),
                column: "code",
                value: "CerberFragment");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("c4993306-2a61-4302-84a0-5825d50d1ae8"),
                column: "code",
                value: "GekkelKilnFragment");

            migrationBuilder.UpdateData(
                table: "Items",
                keyColumn: "id",
                keyValue: new Guid("e2e0c07e-e82d-4c72-8dbe-abb0ec02b86f"),
                column: "code",
                value: "TorilBuiniyFragment");
        }
    }
}
